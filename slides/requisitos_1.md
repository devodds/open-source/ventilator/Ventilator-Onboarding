# 1er Revisión de Requisitos

---

# Visión general
- Revisar convocatoria CONACYT
- Revisar los requisitos del proyecto
- Comenzar la planificación del proyecto

---

## Requisitos de la convocatoria
### Requisito General

- Contar con preinscripción o inscripción definitiva en el RENIECYT

---
## Requisitos de la convocatoria
### Requisitos de Solicitud
- Límite para recepción de solicitudes **27 de abril** de 2020 a las 18:00 h
- Nombre de Proyecto: ????
- Nombre de la Institución: Diseño de Producto mas ingeniería de manufactura ??
- Nombre de los responsables técnico, legal y administrativo: ??
- Antecedentes, justificación, beneficios ...

---
### Requisitos de Solicitud
- Desglose financiero que muestre los rubros solicitados y su justificación
- Grupo de trabajo
- Cronograma de actividades

---
#### Planificación
- Una sola etapa de ejecución 
- No mayor a 6 meses 
- Entre junio a noviembre de 2020

---
#### Documentos
- Versión pública del proyecto
- No tener adeudos económicos con el Conacyt
- Describir el mecanismo por el cual se realizará la transferencia de conocimiento
- Diagnóstico de Madurez de la Tecnológica propuesta


---

### Nivel de Madurez Tecnológica

![](https://docs.google.com/drawings/d/e/2PACX-1vRVlg6rS77vBzJqm3x_CzcujZCDva1Cp6Bu5jv3iohOBbIpleA2NW0Ka2QA8BSclqoT20Kj2Tz9YZus/pub?w=1599&h=1094)

---
### Prototipo
- Integración de componentes básicos 
- Configuración del sistema sea similar a una aplicación final en **casi todas sus características**. 
- En TRL 5, el sistema es **casi prototipo**

---

## Requisitos del proyecto
- Presión controlada: inspiratoria de 40 cm H2O, espiratoria de 25 cm H2O
- Frecuencia respiratoria de 6 a 40 respiraciones por minuto
- Tiempo inspiratorio ajustable o relación I:E ajustable
- Medición del volumen de circulante

---

- FiO2 ajustable de 21% a 100% en incrementos del 10%
- Activación por tiempo o por requerimientos del paciente
- Puede ser conectado a las máscaras estándar, tubos y conectores de oxígeno estándar
- De doble circuito con válvula de no re-inspiración

---
### Presición
- 10% para el volumen / presion
- tolerancia de 1 respiro por minuto

### Tiempo mínimo entre fallas
- 14 días

---
## Requisitos de Prototipo
- Presión fija: inspiratoria de 40 cm H2O
- Frecuencia respiratoria de 6 a 40 respiraciones por minuto
- Tiempo inspiratorio ajustable
- Activación por tiempo 
