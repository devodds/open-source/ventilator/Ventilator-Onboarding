```plantuml
!includeurl https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/master/C4_Context.puml
!includeurl https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/release/1-0/C4_Container.puml

title Ventilator

Person(operator, "Doctor", "Ventilarot operator")
System_Boundary(c1, "Ventilator") {
    System(control_unit, "CPU", "Overall control")
    System(hmi, "HMI", "Human Machine Interface")
    System(motors, "Motors", "Mechanical movement")
    System(ambu, "Ambu", "Ambu bag and tubes")
}

Person(pacient, "Pacient", "Pacient needing mechanical ventillation")

Rel(operator, control_unit, "Set parameters")
Rel(control_unit, operator, "Show parameters")
Rel(control_unit, motors, "Power")
Rel_Back(control_unit, motors, "Read position")
Rel(control_unit, hmi, "Intant values")
Rel(hmi, operator, "Historical values")
Rel(motors, ambu, "Press")
Rel(ambu, pacient, "Positive Preassure Air")
```
