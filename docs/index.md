# Ventilador Contingencia Covid 19

Proyecto destinado al desarrollo de un ventilador para ser utilizado en caso de saturación del sistema de salud.

**IMPORTANTE:** Todos los datos explicados abajo, están **fuertemente** apoyados en los artículos citados en los enlaces de este documento. La lectura de este archivo sin vistar los *links* dejará poco o nada de información útil.


## Carpeta de Google Drive con Información
El siguiente enlace es de una carpeta **pública**  
<https://drive.google.com/open?id=12I8vrzA3WeKm0PD__AE3F645j4XbsBn5>

# Introducción a la temática
## Oxigenoterapia y Ventilación Mecánica
En la siguiente presentación y artículo se muestra las diferecias entre el tratamiento con oxígeno y la ventilación mecánica.

<https://es.slideshare.net/FernandaMucio/oxigenoterapia-y-ventilacin-mecnica>  
<http://mimiruizromero.blogspot.com/2012/11/oxigenoterapia-y-ventilacion-mecanica.html>

En la **oxigenoterápia** se suministra aire con una concetración más alta de oxígeno que la disponible en la atmósfera.

La ventilación mecánica puede ser subdividida, pero básicamente se trata de un sistema en el que se le agrega presión positiva al aire disponible. Es decir, el paciente tendrá que hacer un menor esfuerzo para respirar (o incluso no hacer esfuerzo).
## ¿Cómo funcionan los ventiladores?
La siguiente nota de BBC explica **"cómo funcionan los respiradores"**.

<https://www.bbc.com/mundo/noticias-52060716>

Tal como cita el artículo, los ventiladores ayudan a empujar aire dentro de nuestros pulmones y esto es de vital importancia cuando, debido a infección e inflamación, no es posible que el paciente respire por si solo. Además, el aire empujado por el ventilador puede ser enrequicido con oxígeno. En este último caso, no sólo será más fácil (o posible) respirar, sino que necesitaremos menos aire para absorver la misma cantidad de oxígeno.

En el caso de la enfermedad generada por esta pandemia, no existe una cura. Por lo que es necesario ayudar al paciente a respirar hasta que su cuerpo pueda sanar.

Existen dos tipos de **ventilación mecánica**:
### Ventilación mecánica
En este caso el paciente debe ser "entubado".

El siguiente es un video explicando el funcionamiento de un respirador:  
<https://www.youtube.com/watch?v=L3JfeOy8Zkw>

Curso explicativo de ventilación mecánica. Este es importante para conocer todos los parámetros de estos dispositivos.

1. <https://www.youtube.com/watch?v=gk_Qf-JAL84>  
1. <https://www.youtube.com/watch?v=K0maLgTzIto>  
1. <https://www.youtube.com/watch?v=6Bdv7QhNNy4>  
1. <https://www.youtube.com/watch?v=KHpJ21UWbhg>  
1. <https://www.youtube.com/watch?v=Jx7oeJKzI9g>  

### Ventilación no invasiva
En este caso, se utiliza una mascarilla que se ajusta a la cara del paciente. Estas mascarillas son diferentes a las de oxigenoterapia, porque no pueden permitir que se pierda la presión positiva. Sino, no podrían empujar el aire.

Este tipo de ventilación es ampliamente utilizada en el tratamiento de apneas durante el sueño.

Dentro de la ventilación no invasiva, existen tipos:

<https://medlineplus.gov/spanish/ency/article/001916.htm>
#### Presión Positiva Continua en las vías respiratorias (CPAP)
El objetivo de este tipo es tener abierta las vías respiratorias y facilitar el ingreso de aire.

#### Presión Positiva de Dos Niveles en las vías respiratorias (BiPAP)
A diferencia de los CPAP, aquí hay dos niveles de presión. Alta presión positiva al inhalar y una menor presión para facilitar el momento de exhalación.

# Desarrollo de un ventilador
El siguiente video explica un poco el escenario actual

<https://www.youtube.com/watch?v=7vLPefHYWpY>

Tal como se menicona en el video anterior, muchos de los diseños van apuntados al accionamiento de un bolsa y están basados en el trabajo de un alumno del MIT. En el caso del mundo hispano parlante, hay varios grupos siguiendo este camino:

-  ReesistenciaT
  -  <https://mobile.twitter.com/ReesistenciaT>
  -  <https://gitlab.com/reesistencia/reespirator-doc>
-  Coronavirus Makers MX
  -  <https://www.coronavirusmakers.mx/index.html>
  -  <https://discord.gg/9HDFCK>

Desde el último enlace se puede acceder al canal de chat de un grupo mexicano que está hablando sobre el tema. Aquí se publicó un trabajo sobre un CPAP

- airbreak
  - <https://airbreak.dev/>
  - <https://github.com/osresearch/airbreak/>

Un ejemplo de un prototipo para atender la emergencia de Covid 19

- <https://maingear.com/liv/>

Lista de proyectos que se están llevando a cabo en todo el mundo:

- <https://docs.google.com/spreadsheets/d/e/2PACX-1vTYAfldxoIiO46VAWH1NlhrwFBn9mguqS2bh1spnLEu4AVVN1cj1vaEm6vOp5Z6UnaAbUwd8dslCXdM/pubhtml>

## El ventilador de Medtronics
Medtronic ha liberado todos los archivos de manufactura para su ventilador Medtronic PB560

<https://www.medtronic.com/us-en/e/open-files/thank-you.html>

El análisis de estos archivos, creo que dan un excelente ejemplo sobre los documentos que necesitamos para que el respirador sea fabricado. Más allá de los detalles particulares de este ventilador mecánico, creo que lo importante es aprender de este proyecto y cómo presentar documentos de manufactura, abarcando desde el detalle mecánico, hasta el *software*.

## Sistemas Críticos
En la siguiente nota se habla sobre los conceptos importantes de sistemas de tiempo real y **sistemas críticos**. Nótese que digo sistemas y no *software*. En el caso de estos dispositivos, son sistemas críticos. Es decir, una falla de cualquier componente mecánico, electrónico o de *software* puede causar daño a una persona.

<https://www.freecodecamp.org/news/programming-the-electronics-for-covid-19-ventilators/>
## Noticias sobre el tema
1. <https://innovadores.larazon.es/es/asi-es-respirador-automatico-3d-hospitales-creado-por-makers/>  
1. <https://www.inti.gob.ar/noticias/18-institucional/1616-el-inti-tambien-aporta-soluciones-en-la-emergencia-sanitaria>  
1. <http://portal.udea.edu.co/wps/portal/udea/web/generales/interna/!ut/p/z1/1VRLc9owEP4tHDgaSX6A3JvjmmQIxDwCBF0ysiyMWlt2bJk0_fWVyWQyKa8yaQ_VRY_99tvVfisBAh4AkXQrEqpELmmq9yvSfcSubyLPhsPr6cCHXtf3gt5sMTSxCZYnAGjhO4AcNY8x2vMPb_pYA2zPtdwQzQJ0oT-8RQ70JsH47j70x18H5p_5wyPDgxfG3weQ0_VbAAIIk6pQG7Aq8lLRtI45bUNafdxt8oy_rYWslFA122nUhrUUMY15ZVCmp0wwWjWoNWV1qhpDGwqZcCl42WJWi8Y7EpZLpc_ifAdORaWhuV4yVdNU_KQNu7HlUolUG0oj44xKzW0IKatClDTjcZN8wUQMVjRyncjmPcNxqGvYunZGxCE2egi7lEJt7K5PFmtXDXJai2UT70y7nQL0R93fAQca7iMAQ9zXHXU_DOz5CMJwL8SBljt3jwEgSZpHr-_Lk5GFE0BKvuYlLzt1qY83ShVf2vBdOEE7jfodHtcdpmU65LnJKwUeDnuAla5972jtQwSWW8GfwVzmZaYTm10o7Q08E8GGn4zwSn83cQJk-lo4aAZwEl4NrocjBEPf_Cv0R7NH_5T-9rPZD879M7pdxLenJ-Lp36Z5-D90p_x_302RzZuRYevF-D4NpvjFSreZH-FRYDlJ9vg2VV6r9QtoMZqo/dz/d5/L2dBISEvZ0FBIS9nQSEh/?page=udea.generales.interna&urile=wcm%3Apath%3A%2FPortalUdeA%2FasPortalUdeA%2FasHomeUdeA%2FasInstitucional%2Funidades-academicas%2FasFacultades%2FIngenier%2521c3%2521ada%2FasContenidos%2FasListado%2Factualizacion-ventilador-mecanica-innspiramed>  
1. <https://www.clarin.com/sociedad/universidad-publica-rosario-fabricara-respiradores-atender-demanda-coronavirus_0_hfQ9-6x0G.html?utm_medium=Social&utm_source=Facebook#Echobox=1584897287>  
1. <https://tn.com.ar/autos/lo-ultimo/coronavirus-una-automotriz-espanola-fabrica-respiradores-con-motores-de-limpiaparabrisas_1049883>  
1. <http://sabi.org.ar/wp-content/uploads/2020/03/Recomendacion_SABI_Respiradores-1.pdf>  

