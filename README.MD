# Ventilator onboarding
Proyecto destinado a describir la estructura de proyectos y orientar a nuevos
miembros del equipo como esta la estructura para poder poner a funcionar el 
ventilador mecánico de emergencia.

Dentro del grupo [***Ventilator***](https://gitlab.com/devodds/open-source/ventilator)
se encuentran todos los proyectos que forman parte de este ventilador mecánico.

> `IMPORTANTE:` ESTA DOCUMENTACIÓN ESTÁ EN ELABORACIÓN, compruebe regularmente
los cambios, recomendamos siempre revisar online sin descargar la misma.
Puedes aprovechar también [**configurando las notificaciones de release**](https://docs.gitlab.com/ee/user/profile/notifications.html) 
para estar enterado cuando se realiza un nuevo lanzamiento

[***DevOdds***](http://devodds.com) fue invitada a participar de dos proyectos 
diferentes de respiradores mecánicos por lo que este documento en principio solo
describe las partes en las que la empresa colabora haciendo referencia a las
demás pero no compartiendo sus datos hasta que estos sean liberados por las 
partes involucradas.

## Documentación
La documentación de acceso público se versiona en el ***README*** del directorio 
[***docs***](/docs/README.md), esta documentación se publica con 
[***GitLab Pages***](https://about.gitlab.com/stages-devops-lifecycle/pages/)
para acceso del público en general en la dirección: [***devodds.gitlab.io***](https://devodds.gitlab.io/open-source/ventilador-contingencia-covid-19/)

## Human Machine Interface (HMI)
Dentro del grupo [***Ventilator***](https://gitlab.com/devodds/open-source/ventilator) 
se encuentra un sub-grupo [***Human Machine Interface***](https://gitlab.com/devodds/open-source/ventilator/human-machine-interface)
que contiene los proyectos que forman parte de la interfaz de usuario del 
respirador. Los proyectos que lo conforman son:

- ***[Arduino Library](https://gitlab.com/devodds/open-source/ventilator/human-machine-interface/arduino-library):*** librería para ***Arduino*** que provee interfaz de
comunicación con el ***HMI***
- ***[Operative system](https://gitlab.com/devodds/open-source/ventilator/human-machine-interface/hmi-operative-system):*** es el sistema operativo instalado en la ***Beagle Bone Green***
que esta preparado para funcionar la interfaz de usuario, a su vez este distribuye
la aplicación gráfica ***GUI*** [***Graphical User Interface***](https://gitlab.com/devodds/open-source/ventilator/human-machine-interface/graphical-user-interface)
- ***[Graphical User Interface](https://gitlab.com/devodds/open-source/ventilator/human-machine-interface/graphical-user-interface):*** interfaz gráfica con ***Qt5*** que permite
visualizar las medidas tomadas por el respirador

